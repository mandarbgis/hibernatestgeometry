package lu.schuller.hibernateSTgeometryTesting.entity;

import com.esri.core.geometry.Geometry;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User: schullto
 * Date: 23/12/2014
 * Time: 5:11 PM
 */
@Entity
@Table(name = "myline")
public class MyLine {

    @Id
    @GeneratedValue(generator = "nextRowId")
    @GenericGenerator(name = "nextRowId", strategy = "lu.schuller.hibernateSTgeometry.NextRowIdSequenceGenerator",
            parameters = {
                    @Parameter(name = "schemaOwner", value = "sde"),
                    @Parameter(name = "tableOwner", value = "sde"),
                    @Parameter(name = "tableName", value = "myline")
            })
    private Integer id;
    private String dsg;
    private String stl;

    /*
    @Type(type = "lu.schuller.hibernateSTgeometry.WktStGeometryType")
    @ColumnTransformer(read = "st_astext(shape)", write = "st_geometry(?,300001)")
    */
    @Type(type= "lu.schuller.hibernateSTgeometry.WkbStGeometryType")
    @ColumnTransformer(read = "st_asbinary(shape)", write = "st_geomfromwkb(?,300001)")
    private Geometry shape;

    public Integer getId() {
        return id;
    }

    public void setId(Integer objectid) {
        this.id = objectid;
    }

    public String getDsg() {
        return dsg;
    }

    public void setDsg(String dsg) {
        this.dsg = dsg;
    }

    public String getStl() {
        return stl;
    }

    public void setStl(String stl) {
        this.stl = stl;
    }

    public Geometry getShape() {
        return shape;
    }

    public void setShape(Geometry shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "MyLine{" +
                "objectid=" + id +
                ", dsg='" + dsg + '\'' +
                ", shapeLength='" + (shape!=null?shape.calculateLength2D():"null") +
                ", shape=" + (shape!=null?shape.toString():"null") +
                '}';
    }
}
